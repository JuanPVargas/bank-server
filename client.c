#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>

#define	KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KBRN "\x1B[33m"
#define KBLU "\x1B[34m"
#define KNRM "\x1B[0m"
#define KWHT "\x1B[37m"

int sd;

void readServerMessage(void * ptr){
	int sd;
	char buffer[256];
	memset(buffer,0,256);
	sd = (*(int *)ptr);
	free(ptr);
	while(1){
		while(read(sd,buffer,sizeof(buffer))<0){
			continue;
		}
		printf("%s",buffer);
		if(strcmp(buffer,KWHT"Message From Server:	program exiting, closing connection\n")==0){
			exit(0);
		}
		memset(buffer,0,256);
	}
}
void SigCatcher(int signal){
	write(sd,"exit",strlen("exit"));
	exit(0);
}

int main(int argc, char ** argv){
	pthread_t mythread;
	char input[256];
	int *ptr;
	struct hostent* hp;
	struct sockaddr_in server;
	if(argc!=2){
		printf(KRED"You must enter the name of the server as an argument\n"KWHT);	
		return 0;
	}
	
	if((sd = socket(AF_INET,SOCK_STREAM,0)) < 0){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
	}
	if((hp = gethostbyname(argv[1])) < 0){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
	}
	server.sin_family = AF_INET;
	memcpy(&server.sin_addr,hp->h_addr,hp->h_length);
	server.sin_port = htons(51284);
	//keep trying to connect
	while(connect(sd, (struct sockaddr *)&server, sizeof(server)) != 0){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);	
		sleep(1);
	}
	memset(input,0,sizeof(input));
	snprintf(input,256,KBRN"Connection has been made to server %s using socket %d on port %d\n"KBRN,argv[1],sd,server.sin_port);
	write(0,input,sizeof(input));
	write(0,KRED"Enter exit to close the Client\n"KNRM,strlen(KRED"Enter exit to close the Client\n"KNRM));
	memset(input,0,sizeof(input));
	//create listening thread
	ptr = (int *)malloc(sizeof(int));
	*ptr = sd;
	pthread_create(&mythread,NULL,(void *)&readServerMessage,(void*)ptr);
	signal(SIGINT,SigCatcher);
	signal(SIGTERM,SigCatcher);
	signal(SIGHUP,SigCatcher);
	signal(SIGKILL,SigCatcher);
	signal(SIGTERM,SigCatcher);
	signal(SIGTSTP,SigCatcher);
	signal(SIGQUIT,SigCatcher);
	while(strcmp(input,"exit") != 0){
		memset(input,0,sizeof(input));
		write(0,KRED"Enter Command:	"KWHT,strlen(KRED"Enter Command:	"KWHT));
		scanf(" %[^\n]",input);
		printf(KBRN"Message Sent to Server:	 %s\n",input);
		write(sd,input,strlen(input)+1);
		sleep(2);
	}
	close(sd);
	return 0;
}
