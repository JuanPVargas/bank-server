#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>


struct TokenizerT_ {
    char *position;     //this pointer points to the start of the next token 
};

typedef struct TokenizerT_ TokenizerT;

TokenizerT *TKCreate( char * ts);
void TKDestroy( TokenizerT * tk );
int IsSeparator(char x);
int isNullChar(char x);
void JumpSpaces(TokenizerT *tk);
int isThreeCharOperator(char* input);
int isTwoCharOperator(char * input);
int isCOperator(char *input);
char* OperatorName(char * input);
char * isComment(char *temp);
char * isInside(char *temp);
char *TKGetNextToken( TokenizerT * tk );
int isEscapeChar(char x);
int isFloat(char *input);
int isOctal(char *input);
int isHex(char *input);
int isDecimal(char *input);
int isAlph(char *input);
int isErrorHex(char *input);
int isKeyword(char * input);
char* TKIdentifier(char * input);


#endif