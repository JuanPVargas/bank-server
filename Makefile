COMPILER = gcc
CCFLAGS = -Wall -g 

all: server client
	
tokenizer.o: tokenizer.c tokenizer.h
		$(COMPILER) $(CCFLAGS) -c  tokenizer.c

server: server.c tokenizer.o
		$(COMPILER) $(CCFLAGS) -pthread -o server server.c tokenizer.o
			
client : client.c
		$(COMPILER) $(CCFLAGS) -pthread -o client client.c

clean:
		rm -f *.o
		rm -f client
		rm -f server
