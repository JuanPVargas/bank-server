#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "tokenizer.h"



TokenizerT *TKCreate( char * ts ) {                //creates the tokenizer, and initilize position to the first character of the string.
      TokenizerT *tokenizer = (TokenizerT*)malloc(sizeof(TokenizerT));
      tokenizer->position=ts;

  return tokenizer;
}

void TKDestroy( TokenizerT * tk ) {
    free(tk);                                   //free the memory allocated for the tokenizer
}

int IsSeparator(char x){                            //checks for white spaces to separate the tokens. 
  if(x==0x20 || x==0x09 || x ==0x0b || x==0x0c || x==0x0a || x == 0x0d){
      return 1;
  }else if ( x>0x20 && x < 0x30){
	  return 1;
  }else if(x>0x39 && x < 0x41){
	  return 1;
  }else if(x>0x39 && x < 0x41){
	  return 1;
  }else if(x>0x5a && x < 0x61){
	  return 1;
  }else if (x > 0x7a){
	  return 1;
  }else{	  
      return 0;
  } 
}

int isNullChar(char x){
    if (x==0){                  //checks for end of string. 
        return 1;
    }else{
        return 0;
    }
    
}

void JumpSpaces(TokenizerT *tk){
    while(IsSeparator(*(tk->position))==1){       //jumps white spaces.
         tk->position += 1;     
    }
}





char *TKGetNextToken( TokenizerT * tk ) {
     char * temp; 
    if(*(tk->position)==0 ){
        return NULL;
    }
	
    JumpSpaces(tk);
    temp=tk->position;
	
	int x = IsSeparator(*temp) || isNullChar(*temp);

	while(x!=1){
	temp += 1;
	x = IsSeparator(*temp) || isNullChar(*temp); 	
	}
	int size = temp - tk->position;
	char* token=(char*)malloc(sizeof(char)*size);
	strncpy(token,tk->position,size);
	tk->position=temp; 
    
	if(strcmp(token,"")==0){
		return NULL;
	}
	
	return token;			
			
		
 }
    
 
