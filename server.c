#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <ctype.h>
#include "tokenizer.h"
#include <errno.h>
#include <pthread.h>
#include <sys/wait.h>

#define	KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KBRN "\x1B[33m"
#define KBLU "\x1B[34m"
#define KNRM "\x1B[0m"
#define KWHT "\x1B[37m"

typedef struct account_{
	char name[100];
	float balance;
	pthread_mutex_t accountlock;
}account;

typedef struct bank_{
	int numberOfAccounts;
	account bankAccounts[20];
	pthread_mutex_t banklock;
}bank;

account bankAccounts[20];
bank* shm;

void bank_program(int fd){
	char output[256];
	memset(output,0,256);
	char buffer[256];
	memset(buffer,0,256);
	char accountname[100];
	memset(accountname,0,100);
	int i;
	int quit = 0;
	//write(fd,"Waiting for command:\n",23);
	while(1){
		while(read(fd,buffer,sizeof(buffer))<0){
			continue;
		}
		printf("%s\n",buffer);
		TokenizerT* tokenizer = TKCreate(buffer);
		char * token = TKGetNextToken(tokenizer);
		if(token == NULL){
			write(fd,KWHT"Message From Server:	invalid command\n",strlen(KWHT"Message From Server:	invalid command\n"));
		}
		while(token != NULL){
			char  *p;
			for (p = token; *p != '\0'; p++){
				*p = (char) tolower(*p);
			}
			if(strcmp(token,"open") == 0){			//case open here	
				while(pthread_mutex_trylock(&((*shm).banklock)) != 0){
					write(fd,KWHT"Message From Server:	Waiting for account creation access\n",strlen(KWHT"Message From Server:	Waiting for account creation access\n"));
					snprintf(output,256,KWHT"Process %d is waiting to create an account\n",getpid());
					write(0,output,strlen(output));
					memset(output,0,256);
					sleep(5);
				}
				JumpSpaces(tokenizer);
				if(strcmp(tokenizer->position,"") == 0){
					write(fd,KWHT"Message From Server:	open must be followed with an account name : try again\n",strlen(KWHT"Message From Server:	open must be followed with an account name : try again\n"));			
					pthread_mutex_unlock(&((*shm).banklock));
					break;
				}
				if(strlen(tokenizer->position)>100){
					write(fd,KWHT"Message From Server:	account name must be at most 100 characters: try again\n",strlen(KWHT"Message From Server:	account name must be at most 100 characters: try again\n"));
					pthread_mutex_unlock(&((*shm).banklock));
					break;
				}
				if((*shm).numberOfAccounts == 20){
					write(fd,KWHT"Bank already has the maximum number of accounts\n",strlen(KWHT"Bank already has the maximum number of accounts\n"));
					pthread_mutex_unlock(&((*shm).banklock));
					break;
				}
				for(i = 0; i < (*shm).numberOfAccounts;i++){
					if(strcmp(((*shm).bankAccounts[i]).name,tokenizer->position) == 0){
						i = 20;
						break;
					}
				}
				if(i == 20){
					write(fd,KWHT"Message From Server:	Account name entered already exists : try again\n",strlen(KWHT"Message From Server:	Account name entered already exists : try again\n"));
					pthread_mutex_unlock(&((*shm).banklock));
					break;
				}
				if(pthread_mutex_init(&((*shm).bankAccounts[((*shm).numberOfAccounts)]).accountlock, NULL) != 0){
					write(fd,KWHT"Account creation failed : try again\n",strlen(KWHT"Account creation failed : try again\n"));
					pthread_mutex_unlock(&((*shm).banklock));
					break;
				}
				strcpy(((*shm).bankAccounts[((*shm).numberOfAccounts)]).name,tokenizer->position);
				(*shm).bankAccounts[((*shm).numberOfAccounts)].balance = 0;
				(*shm).numberOfAccounts++;
				write(fd,KWHT"Message From Server:	Account created\n",strlen(KWHT"Message From Server:	Account created\n"));
				pthread_mutex_unlock(&((*shm).banklock));
				break;
			}								//case open ends
			else if(strcmp(token,"start") == 0){	//case start her
				JumpSpaces(tokenizer);
				if(strcmp(tokenizer->position,"") == 0){
					write(fd,KWHT"Message From Server:	start must be followed with an account name : try again\n",strlen(KWHT"Message From Server:	start must be followed with an account name : try again\n"));			
					break;
				}
				if(strlen(tokenizer->position)> 100){
					write(fd,KWHT"Message From Server:	valid account names have at most 100 characters\n",strlen(KWHT"Message From Server:	valid account names have at most 100 characters\n"));
					break;
				}
				for(i = 0; i < (*shm).numberOfAccounts;i++){
					if(strcmp(((*shm).bankAccounts[i]).name,tokenizer->position) == 0){
						break;
					}
				}
				if(i == (*shm).numberOfAccounts){
					write(fd,KWHT"Message From Server:	Account name does not exist: try again\n",strlen(KWHT"Message From Server:	Account name does not exist: try again\n"));
					break;
				}
				while(pthread_mutex_trylock(&(((*shm).bankAccounts[i]).accountlock)) != 0){
					snprintf(output,256,"Message From Server:	Account %s currently in a session with another client: please wait\n",((*shm).bankAccounts[i]).name);
					write(fd,output,strlen(output));
					printf(KWHT"Process %d is waiting to start a session on account %s\n",getpid(),tokenizer->position);
					sleep(5);
				}
				write(fd,KWHT"Message From Server:	Session started\n", strlen(KWHT"Message From Server:	Session started\n"));
				TKDestroy(tokenizer);
				memset(buffer,0,256);
				while(quit == 0){
					while(read(fd,buffer,sizeof(buffer))<0){			
						continue;
					}
					printf("%s\n",buffer);
					TokenizerT* tokenizer = TKCreate(buffer);
					char * token = TKGetNextToken(tokenizer);
					if(token == NULL){
						write(fd,KWHT"Message From Server:	invalid command\n",strlen(KWHT"Message From Server:	invalid command\n"));
						continue;
					}
					while(token != NULL){
						char  *p;
						for (p = token; *p != '\0'; p++){
							*p = (char) tolower(*p);
						}
						if(strcmp(token,"credit") == 0){ //case credit here
							if(strcmp(tokenizer->position,"") == 0){
								write(fd,KWHT"Message From Server:	credit must be followed by a balance\n",strlen(KWHT"Message From Server:	credit must be followed by a balance\n"));
								break;
							}
							if(atof(tokenizer->position)>=0){ //check if value entered is a float
								if(atof(tokenizer->position) < 0 ){  //enter here if amount entered is valid
									write(fd,KWHT"Message From Server:	balance amount to credit must be greater than 0\n",strlen(KWHT"Message From Server:	balance amount to credit must be greater than 0\n"));
									break;
								}
								((*shm).bankAccounts[i]).balance = ((*shm).bankAccounts[i]).balance + atof(tokenizer->position);
								snprintf(output,256,KWHT"Message From Server:	New balance is %.2f on account %s\n",((*shm).bankAccounts[i]).balance,((*shm).bankAccounts[i]).name);
								write(fd, output, sizeof(output));
								break;
							}
							else { // enter here if not a float
								write(fd,KWHT"Message From Server:	balance ammount is invalid : try again\n",strlen(KWHT"Message From Server:	balance ammount is invalid : try again\n"));
								break;
							}
						} //case credit ends here
						else if(strcmp(token,"debit") == 0){ //case debit here 
							if(strcmp(tokenizer->position,"") == 0){
								write(fd,KWHT"Message From Server:	debit must be followed by a balance\n",strlen(KWHT"Message From Server:	debit must be followed by a balance\n"));
								break;
							}
							if(atof(tokenizer->position)>=0){ //enter here if entered a valid float
								if(atof(tokenizer->position) < 0 ){
									write(fd,KWHT"Message From Server:	balance amount to debit must be greater than 0\n",strlen(KWHT"Message From Server:	balance amount to debit must be greater than 0\n"));
									break;
								}
								if(((*shm).bankAccounts[i]).balance - atof(tokenizer->position) < 0){ //check for overdrawing
									write(fd,KWHT"Message From Server:	balance to debit would overdraw the account : try again\n",strlen(KWHT"Message From Server:	balance to debit would overdraw the account : try again\n"));										token = NULL;
									break;
								} //else compute transaction
								((*shm).bankAccounts[i]).balance = ((*shm).bankAccounts[i]).balance - atof(tokenizer->position);
								snprintf(output,256,KWHT"Message From Server:	New balance is %.2f on account %s\n",((*shm).bankAccounts[i]).balance,((*shm).bankAccounts[i]).name);
								write(fd, output, sizeof(output));
								break;
							} //end successful transaction
							else { //enter here if not a float
								write(fd,KWHT"Message From Server:	balance amount is invalid : try again\n",strlen(KWHT"Message From Server:	balance ammount is invalid : try again\n"));
								break;
							}
						} //case debit ends here
						else if(strcmp(token,"balance") == 0){//case balance here
							JumpSpaces(tokenizer);
							if(strcmp(tokenizer->position,"") == 0){
								snprintf(output,256,KWHT"Message From Server:	Balance is %.2f\n",((*shm).bankAccounts[i]).balance);
								write(fd,output,sizeof(output));
								break;
							}
							else{
								write(fd,KWHT"Message From Server:	too many arguments : try again\n",strlen(KWHT"Message From Server:	too many arguments : try again\n"));
								break;
							}
						} //case balance ends here
						else if(strcmp(token,"finish") == 0){ //case finish here
							JumpSpaces(tokenizer);
							if(strcmp(tokenizer->position,"") == 0){
								quit = 1;
								write(fd,KWHT"Message From Server:	session terminated\n",strlen(KWHT"Message From Server:	session terminated\n"));
								pthread_mutex_unlock(&(((*shm).bankAccounts[i]).accountlock));
								break;
							}
							else {
								write(fd,KWHT"Message From Server:	too many arguments : try again\n",strlen(KWHT"Message From Server:	too many arguments : try again\n"));
								break;
							}
						} //case finish ends here
						else if(strcmp(token,"exit") == 0){//case exit here
							JumpSpaces(tokenizer);
							if(strcmp(tokenizer->position,"") == 0){
								pthread_mutex_unlock(&(((*shm).bankAccounts[i]).accountlock));
								write(fd,KWHT"Message From Server:	program exiting, closing connection\n",strlen(KWHT"Message From Server:	program exiting, closing connection\n"));
								close(fd);
								TKDestroy(tokenizer);
								return;
							}
							else {
								write(fd,KWHT"Message From Server:	too many arguments : try again\n",strlen(KWHT"Message From Server:	too many arguments : try again\n"));
								break;
							}
						}//case exit ends here
						else {
							write(fd,KWHT"Message From Server:	invalid command\n",strlen(KWHT"Message From Server:	invalid command\n"));
							//write(fd,"valid arguments are credit, debit, balance, finish , exit\n",strlen("valid arguments are open, start, credit, debit, balance, finish , exit\n"));
							break;
						}
					}
				}
				quit = 0;
				token = NULL;
				break;
			}
			else if(strcmp(token,"exit") == 0){//case exit here
				JumpSpaces(tokenizer);
				if(strcmp(tokenizer->position,"") == 0){
					write(fd,KWHT"Message From Server:	program exiting, closing connection\n",strlen(KWHT"Message From Server:	program exiting, closing connection\n"));
					close(fd);
					TKDestroy(tokenizer);
					return;
				}
				else {
					write(fd,KWHT"Message From Server:	too many arguments : try again\n",strlen(KWHT"Message From Server:	too many arguments : try again\n"));
					break;
				}
			}//case exit ends here
			else {
				write(fd,KWHT"Message From Server:	invalid command\n",strlen(KWHT"Message From Server:	invalid command\n"));
				write(fd,KWHT"Message From Server:	valid arguments are open, start, credit, debit, balance, finish , exit\n",strlen(KWHT"Message From Server:	valid arguments are open, start, credit, debit, balance, finish , exit\n"));
				break;
			}
			token = NULL;//not needed
			break;//not needed
		}
		token = NULL;
		TKDestroy(tokenizer);
		memset(output,0,256);
		memset(accountname,0,100);
		memset(buffer,0,256);
	}
	return;
}
void exitBank(){
	char buffer[256];
	memset(buffer,0,256);
	while(1){
		while(read(0,buffer,sizeof(buffer))<0){
			continue;
		}
		if(memcmp(buffer,"exit",4)==0){
			exit(0);
		}
	}
}

void printBank(){
	int i=0;
	int n=0;
	while(1){
		while(pthread_mutex_trylock(&((*shm).banklock)) != 0){
			printf(KRED"Process %d is waiting for the lock on the bank to print\n"KNRM,getpid());
			sleep(5);
		}
		n = (*shm).numberOfAccounts;
		printf(KBRN"Start of Print: Bank Accounts\n"KNRM);
		for(i = 0; i < n; i++){
			printf("Account %d: \n \t Name: %s \n \t Balance: %.2f \n",i,((*shm).bankAccounts[i]).name,((*shm).bankAccounts[i]).balance);
		}
		printf(KBRN"End of Print: Bank Accounts\n"KNRM);
		pthread_mutex_unlock(&((*shm).banklock));
		sleep(20);
	}
}

void SigCatcher(int signal){
  wait3(NULL,WNOHANG,NULL);
  printf(KRED"Child process has been reaped\n");
  return ;
}

int main(int argc, char ** argv){
    int sd, fd, pid, i;
	pthread_t mythread;
	pthread_t exitthread;
	struct sockaddr_in server;
	int ic = 1;
	int shmid;
	key_t key;
	signal(SIGCHLD,SigCatcher);
	
	//set up sockaddr_in
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=INADDR_ANY;
	server.sin_port=htons(51284);
	
	//create shared memory segment and get pointer
	if((key = ftok("/ilab/users/dh624/CS214/pa5",25)) == -1){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
	}
	if((shmid = shmget(key, sizeof(bank), 0666 |IPC_CREAT | IPC_EXCL )) == -1){
		if(!(errno == EEXIST)) {
			printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
			return 1;
		}
		if((shmid = shmget(key,0,0666)) == -1){
			printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
			return 1;
		}
	}
	if ((shm = shmat(shmid, NULL, 0)) == (bank *) -1) {
        printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
    }
	//create socket
	if((sd = socket(AF_INET,SOCK_STREAM,0)) < 0){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
	}
    if(setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,(char*)&ic,sizeof(ic)) < 0){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
	}	
	if(bind(sd,(struct sockaddr*)&server,sizeof(server)) < 0){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
	}
	
	if(listen(sd,20) < 0){
		printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		return 1;
	}
	//initialize the bank mutex
	pthread_mutex_init(&((*shm).banklock),NULL);
	//reinitialize all active account mutexs
	for(i=0;i<(*shm).numberOfAccounts;i++){
		pthread_mutex_init(&((*shm).bankAccounts[i]).accountlock, NULL);
	}
	printf(KRED"Starting Bank\n"KNRM);
	//create printing thread
	pthread_create(&mythread,NULL,(void *)&printBank,NULL);
	pthread_create(&exitthread,NULL,(void *)&exitBank,NULL);
	while((fd = accept(sd,(struct sockaddr*)0,0)) != -1){
		printf(KGRN"Connection made with socket descriptor %d\n"KGRN,fd);			
		pid = fork();
		if (pid < 0){
			printf(KRED"errno value %d : Message %s : Line %d\n"KNRM,errno, strerror(errno), __LINE__);
		} 
		if (pid == 0){
			printf(KGRN"Child Process %d created to handle client on socket %d\n",getpid(),fd);	  
			bank_program(fd);
			close(fd);
			printf(KGRN"Child Process %d finish session with client...closing\n",getpid());
			return 0;
		}else{
			 close(fd);
			 //printf("parent process\n");
		}
	}
	return 0;
}
